#!/usr/bin/env node

const clipboardy = require('clipboardy');
const { exec } = require('child_process');

const input = clipboardy.readSync();
const p = exec('oats', (error, stdout, stderr) => {
    if (error) {
        console.error(`Failed to run oats`, error);
        return;
    }
    clipboardy.writeSync(stdout);
});
p.stdin.write(input);
p.stdin.end();
