const { execSync } = require('child_process');

const onWindows = process.platform === 'win32';

try {
    const globalOatsCheckRes = execSync(onWindows ? 'where oats' : 'which oats');
    if (!globalOatsCheckRes.includes('node_modules/.bin')) {
        console.error('[WARNING] ⚠️ Detected globally installed oats. This may cause issues!');
    }
} catch {
    // ok, oats not found
}
