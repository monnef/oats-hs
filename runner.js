#!/usr/bin/env node

const { spawnSync } = require('child_process');

const onWindows = process.platform === 'win32';

// console.log('runner', process.env.PWD, process.argv);

const ret = spawnSync('node_modules/oats-hs/bin/oats' + (onWindows ? '.exe' : ''), process.argv.slice(2), { stdio: 'inherit' });

process.exit(ret.status);
