# oats-hs

This NPM package serves as a local installer of [oats](https://gitlab.com/monnef/oats).

# Requirements

* [NodeJS](https://nodejs.org)
* [Stack](http://haskellstack.org)
* [Git](https://git-scm.com/)
