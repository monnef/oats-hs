const { echo, pwd, mkdir, which, cd, test, config, cp, rm } = require('shelljs');
const { last, pipe, split, head, filter } = require('ramda');
const chalk = require('chalk');

const { version } = require('./package.json');
const { crash, execOrCrash } = require('./common');

const repoDir = 'repo';
const binDependencies = [
    { cmd: 'git' },
    { cmd: 'stack', message: 'Please install it from https://haskellstack.org.' },
];
const onWindows = process.platform === 'win32';
const outputDir = 'bin';

if (process.env.OATSHS_VERBOSE) config.verbose = true;

const checkDependencies = () => {
    binDependencies.forEach(({ cmd, message }) => {
        if (!which(cmd)) crash(`Dependency '${cmd}' not found. ${message || ''}`);
    });
};


const cloneRepoIfMissing = () => {
    const repoExists = test('-d', repoDir);
    if (!repoExists) {
        echo(`repo dir doesn't exist, cloning`)
        execOrCrash(`git clone https://gitlab.com/monnef/oats.git "${repoDir}"`, '`Failed to clone repository.`');
    }
};

const changeDirToRepo = () => {
    cd(repoDir);
    if (!pwd().endsWith(repoDir)) crash(`Unexpected current directory: '${pwd()}', repoDir = '${repoDir}'`);
};

const getTags = () => execOrCrash('git tag').stdout.split('\n').filter(x => x);

const main = () => {
    const textOats = chalk.cyanBright('oats');
    const textOatsHs = chalk.cyanBright('oats-hs');
    echo(`Installing ${textOatsHs}.`);
    echo(`pwd = ${pwd().toString()}`);
    checkDependencies();
    cloneRepoIfMissing();
    changeDirToRepo();
    execOrCrash('git reset --hard');
    execOrCrash('git checkout master');
    const tagName = `v${version}`;
    // handles case when remote tag was moved and doesn't match local one
    if (getTags().includes(tagName)) execOrCrash(`git tag -d ${tagName}`);
    execOrCrash('git fetch --all --tags --prune');
    const tags = getTags();
    if (!tags.includes(tagName)) crash(`Tag ${tagName} not found in ${JSON.stringify(tags)}`);
    execOrCrash(`git -c advice.detachedHead=false checkout tags/${tagName}`);
    execOrCrash('stack clean');
    execOrCrash('stack build');
    const pathToBin = pipe(
        execOrCrash,
        x => x.stdout.trim(),
        split(/\r?\n/g),
        filter(x => x.includes('.stack-work')),
        head,
        x => x.trim(),
    )(onWindows ? 'stack exec -- where oats' : 'stack exec -- which oats');
    echo(`pathToBin = '${pathToBin}'`);
    const binName = last(pathToBin.split(/[\\\/]/));
    echo(`binName = '${binName}'`);
    mkdir('-p', `../${outputDir}`);
    const pathToCopiedBinaryRelativeToRepo = `../${outputDir}/${binName}`;
    if (test('-f', pathToCopiedBinaryRelativeToRepo)) {
        echo(`Deleting old binary '${pathToCopiedBinaryRelativeToRepo}'.`);
        rm(pathToCopiedBinaryRelativeToRepo);
    }
    if (test('-f', pathToCopiedBinaryRelativeToRepo)) crash(`Failed to delete old binary '${pathToCopiedBinaryRelativeToRepo}'`);
    cp(pathToBin, `../${outputDir}/`);
    cd('..');
    const pathToCopiedBinary = `${outputDir}/${binName}`;
    if (!test('-f', pathToCopiedBinary)) crash(`Sanity check failed, file '${pathToCopiedBinary}' not found.`);
    echo(`New binary present at '${pathToCopiedBinary}'.`);
    cd(outputDir);
    const versionCheckRes = execOrCrash(`${onWindows ? '' : './'}${binName} --version`);
    const parsedVersion = versionCheckRes.stdout.trimEnd().split('#')[0];
    const versionOfInstalledBinary = 'v' + parsedVersion;
    if (versionOfInstalledBinary !== tagName) {
        echo('versionCheckRes = ' + JSON.stringify(versionCheckRes));
        echo('versionCheckRes.stdout = ' + versionCheckRes.stdout);
        echo('versionCheckRes.stderr = ' + versionCheckRes.stderr);
        echo('versionCheckRes.code = ' + versionCheckRes.code);
        const errMsg = `Version check failed. Got '${versionOfInstalledBinary}', expected '${tagName}'.`;
        echo(chalk.red('[FAIL]') + ' ' + chalk.bgRed.whiteBright(errMsg));
        crash(errMsg);
    } else {
        echo(chalk.green('[SUCCESS]') + ' ' + textOats + ' installed.');
    }
    cd('..');
};

main();
