const { echo, exec } = require('shelljs');

const crash = (x) => { throw new Error(x); };

const echoErr = (msg) => console.error(msg);

const handleExecError = (ret, msg) => {
    if (ret.code !== 0) {
        echo(JSON.stringify(ret));
        crash(msg);
    }
    return ret;
}

const execOrCrash = (cmd, errorMsg, options) => handleExecError(exec(cmd, options), errorMsg || `Failed to execute: ${cmd}`);

module.exports = {
    crash,
    echoErr,
    execOrCrash,
};
